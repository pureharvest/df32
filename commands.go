package df32

import (
	"bytes"
	"io"
)

type CleanWriter struct {
	Out io.Writer
}

func (w CleanWriter) Write(data []byte) (n int, err error) {
	cleaned := bytes.Map(func(r rune) rune {
		if r >= 32 && r < 127 {
			return r
		}
		return -1
	}, data)

	return w.Out.Write(cleaned)
}
