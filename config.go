package df32

import (
	"bufio"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

type Config struct {
	SearchPath []string
}

var envRegexp = regexp.MustCompile("%([^% ]+)%")

func getDFEnv(envFile string, cfg Config) Config {

	dfenv, err := ioutil.ReadFile(envFile)

	if err != nil {
		return cfg
	}

	for _, line := range strings.Split(string(dfenv), "\n") {
		if strings.HasPrefix(line, "DFPATH") {
			dfpath := envRegexp.ReplaceAllString(strings.TrimPrefix(line, "DFPATH="), "$$$1")
			dfpath = os.ExpandEnv(dfpath)

			cfg.SearchPath = filepath.SplitList(dfpath)
		}
	}

	return cfg
}

func parseEnvFile(r io.Reader, cfg Config) (Config, error) {

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		line := scanner.Text()

		if strings.HasPrefix(line, "DFPATH") {
			dfpath := envRegexp.ReplaceAllString(strings.TrimPrefix(line, "DFPATH="), "$$$1")
			dfpath = os.ExpandEnv(dfpath)

			cfg.SearchPath = filepath.SplitList(dfpath)
		}

	}

	if err := scanner.Err(); err != nil {
		return cfg, err
	}

	return cfg, nil
}

func ReadConfigFromEnvironment() (Config, error) {

	cfg := Config{
		SearchPath: filepath.SplitList(os.Getenv("DFPATH")),
	}

	if len(cfg.SearchPath) == 0 {
		cfg.SearchPath = []string{"."}
	}

	if dfenv := os.Getenv("DFENV"); dfenv != "" {

		envFile, err := os.Open(dfenv)

		if err != nil {
			return cfg, err
		}

		defer envFile.Close()

		return parseEnvFile(envFile, cfg)
	}

	return cfg, nil
}
