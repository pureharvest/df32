package df32

type Driver interface {
	Init(paths []string) error
	LoadTable(name string) (Table, error)
}
