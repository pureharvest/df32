package df32

import (
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

type Environment struct {
	Config
	Filelist Filelist
	drivers  map[string]Driver
}

func NewEnvironment(paths ...string) Environment {

	env := Environment{
		Config: Config{SearchPath: paths},
	}

	return env
}

func (env *Environment) LoadDriver(name string, driver Driver) error {

	env.drivers[name] = driver

	return nil
}

func (env *Environment) WriteFileDefinition(definition FileDefinition) error {

	path := searchPath(definition.Name+".fd", env.Config.SearchPath)

	if path == "" {
		path = filepath.Join(env.Config.SearchPath[0], definition.Name+".fd")
	}

	file, err := os.Create(path)

	if err != nil {
		return errors.Wrap(err, "could not write file definition")
	}

	defer file.Close()

	return errors.Wrap(definition.WriteTo(file), "could not write file definition")
}

func (env *Environment) GetFileDefinitionByName(name string) (FileDefinition, error) {

	path := searchPath(name+".fd", env.Config.SearchPath)

	if path == "" {
		return FileDefinition{}, errors.New("file definintion not found")
	}

	file, err := os.Open(path)

	if err != nil {
		return FileDefinition{}, errors.Wrap(err, "could not read file defintion file")
	}

	defer file.Close()

	fd, err := NewFileDefinitionFromReader(file)

	return fd, errors.Wrap(err, "could not read file definition")

}

// SearchPath will search the environment for filename
func (env Environment) SearchPath(filename string) string {
	return searchPath(filename, env.Config.SearchPath)
}

func (env Environment) LoadFilelistEntry(entry FilelistEntry) (Table, error) {

	driverName := entry.Driver

	if driverName == "OTHER" {
		var err error

		path := env.SearchPath(entry.Filename())

		driverName, err = getDriverFromIntermediateFile(path)

		if err != nil {
			return Table{}, err
		}
	}

	driver, found := env.drivers[driverName]

	if !found {
		return Table{}, errors.New("unsupported driver")
	}

	return driver.LoadTable(entry.Name)

}

func getDriverFromIntermediateFile(path string) (string, error) {

	lines, err := ReadIntermediateFile(path)

	if err != nil {
		return "", err
	}

	for _, line := range lines {
		if line.Name == "DRIVER_NAME" {
			return line.Value, nil
		}
	}

	return "", errors.New("intermediate file does not contain driver name")

}

// searchPath returns the first absolute path of file if it exists in the
// search paths, otherwise it returns an empty string
func searchPath(file string, paths []string) string {
	for _, p := range paths {
		path := filepath.Join(filepath.Clean(p), file)
		if _, err := os.Stat(path); !os.IsNotExist(err) {
			return path
		}
	}

	return ""
}
