package df32

import (
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

func (env *Environment) Command(command string, arg ...string) *exec.Cmd {

	cmd := exec.Command("dfrun", append([]string{command}, arg...)...)

	cmd.Env = append(os.Environ(), "DFPATH="+strings.Join(env.Config.SearchPath, string(filepath.ListSeparator)))

	return cmd
}
