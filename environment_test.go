package df32

import (
	"fmt"
	"os"
	"testing"

	"path/filepath"

	"github.com/stretchr/testify/assert"
)

func Test_Commands(t *testing.T) {

	env := Environment{
		Config: Config{SearchPath: []string{".", "/test"}},
	}

	cmd := env.Command("dfadmin", "-i", "test")

	assert.Equal(t, append(os.Environ(), fmt.Sprintf("DFPATH=%s%s%s", ".", string(filepath.ListSeparator), "/test")), cmd.Env)
	assert.Equal(t, []string{"dfadmin", "-i", "test"}, cmd.Args[1:])
}
