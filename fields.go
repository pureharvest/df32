package df32

import (
	"fmt"
	"strconv"
	"strings"
)

func parseFieldSize(f string) (length int, precision int, err error) {

	parts := strings.Split(f, ".")

	length, err = strconv.Atoi(parts[0])

	if err != nil {
		return 0, 0, err
	}

	if len(parts) > 1 {

		precision, err = strconv.Atoi(parts[1])

		if err != nil {
			return 0, 0, err
		}
	}

	return length, precision, nil
}

type FieldRelation struct {
	File      int
	Field     int
	FileName  string
	FieldName string
}

func (f FieldRelation) String() string {
	if f.File == 0 {
		return ""
	}

	if f.FileName == "" {
		f.FileName = fmt.Sprintf("FILE%d", f.File)
	}

	if f.FieldName == "" {
		f.FieldName = fmt.Sprintf("FIELD_%d", f.Field)
	}

	return fmt.Sprintf("%s.%s (%d,%d)", f.FileName, f.FieldName, f.File, f.Field)
}

type Field struct {
	Name      string
	Number    int
	Type      Type
	Length    int
	Precision int
	RelatesTo FieldRelation `json:"relates_to"`
	Offset    int
	Index     int `json:"use_index"`
}

func (f Field) SizeString() string {

	if f.Type != NumberType {
		return fmt.Sprintf("%d", f.Length)
	}

	return fmt.Sprintf("%d.%d", f.Length, f.Precision)
}

func (f Field) Definition() FieldDefinition {
	return FieldDefinition{
		Name:   f.Name,
		Number: f.Number,
		Type:   convertToFileDefinitionType(f.Type),
	}
}

func (f Field) NeedsIntermediateLength() bool {
	return (f.Type == "number" && f.Precision == 0 && f.Length <= 6) || (f.Type == TextType && f.Length != 0)
}

func (f Field) NeedsIntermediate() bool {
	return f.RelatesTo.File != 0 ||
		f.Index != 0 ||
		f.NeedsIntermediateLength()
}
