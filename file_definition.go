package df32

import (
	"fmt"
	"io"
	"strconv"
	"strings"

	"bufio"
	"regexp"

	"github.com/pkg/errors"
)

// FileDefinition is the basic building block for tables in Dataflex.
type FileDefinition struct {
	Number int
	Name   string
	Fields []FieldDefinition
}

type FieldDefinition struct {
	Number int
	Name   string
	Type   string
}

func NewFileDefinitionFromTable(t Table) (FileDefinition, error) {
	fields := make([]FieldDefinition, 0, len(t.Fields)+1)

	offset := 0

	if len(t.Fields) > 0 && !strings.EqualFold(t.Fields[0].Name, "recnum") {
		offset = 1
		fields = append(fields, FieldDefinition{Number: 0, Name: "RECNUM", Type: "N"})
	}

	fd := FileDefinition{
		Number: int(t.Number),
		Name:   t.Name,
		Fields: fields,
	}

	for i, field := range t.Fields {
		fd.Fields = append(fd.Fields, FieldDefinition{Number: i + offset, Name: field.Name, Type: convertToFileDefinitionType(field.Type)})
	}

	return fd, nil
}

func NewFileDefinitionFromReader(r io.Reader) (FileDefinition, error) {

	fd := FileDefinition{}

	n, err := fmt.Fscanf(r, "#REPLACE FILE%d %s\n", &fd.Number, &fd.Name)

	if err != nil {
		return fd, errors.Wrap(err, "could not load file definition")
	}

	if n != 2 {
		return fd, errors.New("invalid file definition. wrong header")
	}

	regexpString := fmt.Sprintf(`#REPLACE %s\.(\w+) \|F([SND])%d,(\d+)`, fd.Name, fd.Number)

	fieldRegexp := regexp.MustCompile(regexpString)

	scanner := bufio.NewScanner(r)

	for scanner.Scan() {
		line := scanner.Text()

		results := fieldRegexp.FindStringSubmatch(line)

		if len(results) != 4 {
			return fd, errors.Errorf("unexpected line. (%s)", line)
		}

		field := FieldDefinition{
			Name: results[1],
			Type: results[2],
		}

		field.Number, err = strconv.Atoi(results[3])

		if err != nil {
			return fd, errors.Wrapf(err, "unexpected conversion error. %s", line)
		}

		fd.Fields = append(fd.Fields, field)
	}

	return fd, nil
}

func (fd FileDefinition) WriteTo(w io.Writer) error {

	_, err := fmt.Fprintf(w, "#REPLACE FILE%d %s\n", fd.Number, fd.Name)

	if err != nil {
		return errors.Wrap(err, "could not write file definition")
	}

	for _, field := range fd.Fields {
		_, err := fmt.Fprintf(w, "#REPLACE %s.%s |F%s%d,%d\n", fd.Name, field.Name, field.Type, fd.Number, field.Number)

		if err != nil {
			return errors.Wrap(err, "could not write file definition")
		}
	}

	return nil
}

func convertToFileDefinitionType(t Type) string {
	switch t {
	case AsciiType:
		return "S"
	case TextType:
		return "S"
	case NumberType:
		return "N"
	case DateType:
		return "D"
	}

	return ""
}
