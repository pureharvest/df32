package df32

import (
	"bytes"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ReadFileDefinitionFromFile(t *testing.T) {
	tests := []struct {
		name        string
		definition  []byte
		expected    FileDefinition
		expectedErr error
	}{
		{
			definition: []byte(`#REPLACE FILE1 TEST
#REPLACE TEST.RECNUM |FN1,0
#REPLACE TEST.FIELD1 |FS1,1
#REPLACE TEST.FIELD2 |FD1,2
#REPLACE TEST.FIELD3 |FN1,3
`),
			expected: FileDefinition{
				Number: 1,
				Name:   "TEST",
				Fields: []FieldDefinition{
					{Number: 0, Name: "RECNUM", Type: "N"},
					{Number: 1, Name: "FIELD1", Type: "S"},
					{Number: 2, Name: "FIELD2", Type: "D"},
					{Number: 3, Name: "FIELD3", Type: "N"},
				},
			},
			expectedErr: nil,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			fd, err := NewFileDefinitionFromReader(bytes.NewReader(test.definition))

			assert.Equal(t, test.expectedErr, err)
			assert.Equal(t, test.expected, fd)
		})
	}
}

func Test_FileDefinitionWriteTo(t *testing.T) {

	tests := []struct {
		name     string
		fd       FileDefinition
		expected string
	}{
		{
			name: "basic table",
			fd: FileDefinition{
				Number: 20,
				Name:   "TEST",
				Fields: []FieldDefinition{
					{Number: 0, Name: "RECNUM", Type: "N"},
					{Number: 1, Name: "FIELD1", Type: "S"},
					{Number: 2, Name: "FIELD2", Type: "D"},
					{Number: 3, Name: "FIELD3", Type: "N"},
				},
			},
			expected: `#REPLACE FILE20 TEST
#REPLACE TEST.RECNUM |FN20,0
#REPLACE TEST.FIELD1 |FS20,1
#REPLACE TEST.FIELD2 |FD20,2
#REPLACE TEST.FIELD3 |FN20,3
`,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			var buf bytes.Buffer

			if err := test.fd.WriteTo(&buf); err != nil {
				t.Fatal(err)
			}

			assert.Equal(t, test.expected, buf.String())
		})
	}
}

func Test_FileDefinition(t *testing.T) {

	tests := []struct {
		name     string
		table    Table
		expected FileDefinition
	}{
		{
			name: "basic table",
			table: Table{
				Number: 20,
				Name:   "TEST",
				Fields: []Field{
					{Name: "FIELD1", Type: "ascii"},
					{Name: "FIELD2", Type: "date"},
					{Name: "FIELD3", Type: "number"},
				},
			},
			expected: FileDefinition{
				Number: 20,
				Name:   "TEST",
				Fields: []FieldDefinition{
					{Number: 0, Name: "RECNUM", Type: "N"},
					{Number: 1, Name: "FIELD1", Type: "S"},
					{Number: 2, Name: "FIELD2", Type: "D"},
					{Number: 3, Name: "FIELD3", Type: "N"},
				},
			},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			fd := test.table.FileDefinition()

			assert.Equal(t, test.expected, fd)
		})
	}
}
