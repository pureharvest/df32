package df32

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strings"

	"github.com/pkg/errors"
)

type FilelistEntry struct {
	Number       int64
	Driver       string
	Name         string
	DataflexName string
	DisplayName  string
}

func (f FilelistEntry) RootName() string {
	if f.Driver == "" || f.Driver == "DATAFLEX" {
		return f.Name
	}

	return fmt.Sprintf("%s.int", f.Name)
}

func (f FilelistEntry) Filename() string {
	if f.Driver == "" || f.Driver == "DATAFLEX" {
		return f.Name + ".dat"
	}

	return f.Name + ".int"
}

func (f FilelistEntry) Validate() error {

	if len(f.RootName()) > rootLen {
		return errors.Errorf("root name must be less that %d", rootLen)
	}

	if len(f.DisplayName) > displayLen {
		return errors.Errorf("display name must be less that %d", displayLen)
	}

	if len(f.DataflexName) > dataflexLen {
		return errors.Errorf("dataflex name must be less that %d", dataflexLen)
	}

	return nil
}

func (entry FilelistEntry) Bytes() []byte {

	if entry.DataflexName == "" {
		entry.DataflexName = entry.Name
	}
	return []byte(fmt.Sprintf(
		"%s%s%s%s%s%s",
		entry.RootName(), strings.Repeat("\x00", rootLen-len(entry.RootName())),
		entry.DisplayName, strings.Repeat("\x00", displayLen-len(entry.DisplayName)),
		entry.DataflexName, strings.Repeat("\x00", dataflexLen-len(entry.DataflexName)),
	))
}

const (
	maxEntries  = 4096
	entrySize   = 128
	rootLen     = 41
	displayLen  = 33
	dataflexLen = 54
)

const MaxEntries = maxEntries

func NewFilelist(filename string, entries ...FilelistEntry) (Filelist, error) {
	list := Filelist{
		Filename: filename,
		data:     make([]byte, maxEntries*entrySize),
	}

	for _, entry := range entries {
		if err := list.Set(entry); err != nil {
			return Filelist{}, err
		}
	}

	return list, nil
}

func NewFromReader(filename string, r io.Reader) (Filelist, error) {

	filelist, err := NewFilelist(filename)

	if err != nil {
		return filelist, err
	}
	data, err := ioutil.ReadAll(r)

	if err != nil {
		return Filelist{}, errors.Wrap(err, "could not read filelist")
	}

	if len(data)%entrySize != 0 {
		return Filelist{}, errors.New("could not read filelist")
	}

	copy(filelist.data, data)

	return filelist, nil
}

func Open(filename string) (Filelist, error) {
	f, err := os.Open(filename)

	if err != nil {
		return Filelist{}, err
	}

	defer f.Close()

	return NewFromReader(filename, f)
}

type Filelist struct {
	Filename string
	data     []byte
}

func (f *Filelist) NextAvailableNumber() int {
	entry := 1

	for entry < maxEntries {
		if f.data[entry*entrySize] == 0 {
			return entry
		}
	}

	return 0
}

func readString(data []byte) string {
	length := bytes.IndexByte(data, byte(0))
	if length == -1 {
		length = len(data)
	}

	return string(data[:length])
}

// Get retrieves the entry at i
func (f *Filelist) Get(i int) (FilelistEntry, error) {

	if i > maxEntries {
		return FilelistEntry{}, errors.Errorf("filelist only supports %d entries", maxEntries)
	}

	entry := FilelistEntry{
		Number: int64(i),
		Driver: "DATAFLEX",
	}

	offset := i * entrySize
	displayOffset := offset + rootLen
	dataflexOffset := displayOffset + displayLen

	rootName := readString(f.data[offset : offset+rootLen])
	entry.DisplayName = readString(f.data[displayOffset : displayOffset+displayLen])
	entry.DataflexName = readString(f.data[dataflexOffset : dataflexOffset+dataflexLen])

	s := strings.Split(rootName, ":")

	if len(s) == 2 {
		entry.Driver = s[0]
		entry.Name = s[1]
	} else if strings.HasSuffix(strings.ToUpper(s[0]), ".INT") {
		entry.Driver = "OTHER"
		entry.Name = strings.TrimSuffix(strings.ToUpper(s[0]), ".INT")
	} else {
		entry.Name = s[0]
	}

	return entry, nil
}

func (f *Filelist) GetAll() []FilelistEntry {
	var entries []FilelistEntry

	for i := 1; i < maxEntries; i++ {
		entry, err := f.Get(i)

		if err != nil {
			continue
		}

		if entry.Name != "" {
			entries = append(entries, entry)
		}
	}

	return entries
}

func (f *Filelist) GetByName(name string) (FilelistEntry, error) {
	for i := 1; i < maxEntries; i++ {
		entry, err := f.Get(i)

		if err != nil {
			return FilelistEntry{}, err
		}

		if strings.EqualFold(name, entry.Name) {
			return entry, nil
		}
	}

	return FilelistEntry{}, errors.New("filelist entry not found")

}

func (f *Filelist) Set(entry FilelistEntry) error {

	if entry.Number >= maxEntries {
		return errors.Errorf("filelist only supports %d entries", maxEntries)
	}

	if err := entry.Validate(); err != nil {
		return errors.Wrap(err, "invalid filelist entry")
	}

	offset := entry.Number * entrySize
	copy(f.data[offset:offset+entrySize], entry.Bytes())

	return nil
}

func (f *Filelist) Delete(i int) error {
	if i >= maxEntries {
		return errors.Errorf("filelist only supports %d entries", maxEntries)
	}

	offset := i * entrySize
	copy(f.data[offset:offset+entrySize], bytes.Repeat([]byte{0}, entrySize))

	return nil
}

func (filelist *Filelist) WriteTo(wr io.Writer) (int, error) {

	n, err := wr.Write(filelist.data)

	if err != nil {
		return n, errors.Wrap(err, "could not write filelist")
	}
	if n != len(filelist.data) {
		return n, errors.New("could not write filelist")
	}

	return n, nil
}

func (filelist *Filelist) Save() error {
	f, err := os.Create(filelist.Filename)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = filelist.WriteTo(f)

	return err
}
