package df32

import (
	"bytes"
	"testing"

	"github.com/pkg/errors"

	"github.com/stretchr/testify/assert"
)

func Test_ReadFilelist(t *testing.T) {

	filelist, err := Open("./test-fixtures/filelist.cfg")

	entry, err := filelist.Get(1)

	if err != nil {
		t.Fatal(err)
	}

	assert.Equal(t, FilelistEntry{Number: 1, Driver: "DATAFLEX", Name: "TEST", DisplayName: "Test File", DataflexName: "TEST"}, entry)
}

func Test_FilelistFromReader(t *testing.T) {

	tests := []struct {
		name     string
		input    []byte
		filelist Filelist
		err      error
	}{
		{
			name:  "empty reader",
			input: []byte{},
			filelist: Filelist{
				Filename: "filelist.cfg",
				data:     make([]byte, entrySize*maxEntries),
			},
			err: nil,
		},
		{
			name:     "partial reader fails",
			input:    []byte{0, 0, 0},
			filelist: Filelist{},
			err:      errors.New("could not read filelist"),
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {

			buf := bytes.NewReader(test.input)

			filelist, err := NewFromReader("filelist.cfg", buf)

			if test.err != nil {
				assert.Equal(t, test.err.Error(), err.Error())
			}
			assert.Equal(t, test.filelist, filelist)

		})
	}
}

func makeByteSlice(val string, len int) []byte {
	s := make([]byte, len)

	copy(s, val)

	return s
}

func makeByteEntry(rootName, displayName, dataflexName string) []byte {
	s := make([]byte, entrySize)

	copy(s[:rootLen], []byte(rootName))
	copy(s[rootLen:rootLen+displayLen], []byte(displayName))
	copy(s[rootLen+displayLen:rootLen+displayLen+dataflexLen], []byte(dataflexName))

	return s
}

func Test_WriteFilelist(t *testing.T) {
	filelist, _ := NewFilelist("filelist.cfg")

	entry := FilelistEntry{Number: 1, Driver: "DATAFLEX", Name: "TEST", DisplayName: "Test File", DataflexName: "TEST"}
	filelist.Set(entry)

	var buf bytes.Buffer

	_, err := filelist.WriteTo(&buf)

	if err != nil {
		t.Fatal(err)
	}

	output := buf.Bytes()

	assert.Equal(t, makeByteSlice(entry.RootName(), rootLen), output[128:169])
	assert.Equal(t, makeByteSlice(entry.DisplayName, displayLen), output[169:202])
	assert.Equal(t, makeByteSlice(entry.DataflexName, dataflexLen), output[202:256])
}

func Test_ReadIntFileEntries(t *testing.T) {

	data := make([]byte, maxEntries*entrySize)

	value := makeByteEntry("FILE.int", "File Description", "FILE")

	copy(data[entrySize:], value)

	filelist, err := NewFromReader("filelist.cfg", bytes.NewReader(data))

	if err != nil {
		t.Fatal(err)
	}

	entry, err := filelist.Get(1)

	assert.Nil(t, err)
	assert.Equal(t, FilelistEntry{Number: 1, Driver: "OTHER", Name: "FILE", DisplayName: "File Description", DataflexName: "FILE"}, entry)

}

func Test_OnlyReadsUpToFirstNull(t *testing.T) {
	data := make([]byte, maxEntries*entrySize)

	value := makeByteEntry("FILE\x00 :tes", "File Description", "FILE")

	copy(data[entrySize:], value)

	filelist, err := NewFromReader("filelist.cfg", bytes.NewReader(data))

	if err != nil {
		t.Fatal(err)
	}

	entry, err := filelist.Get(1)

	assert.Nil(t, err)
	assert.Equal(t, FilelistEntry{Number: 1, Driver: "DATAFLEX", Name: "FILE", DisplayName: "File Description", DataflexName: "FILE"}, entry)
}
