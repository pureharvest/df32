module gitlab.com/pureharvest/df32

go 1.16

require (
	github.com/golang-sql/civil v0.0.0-20190719163853-cb61b32ac6fe
	github.com/pkg/errors v0.9.1
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.7.0
)
