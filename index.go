package df32

type Index struct {
	Number     int
	Name       string
	Fields     []IndexField
	Mode       string
	Length     int64
	Levels     int64
	PrimaryKey bool
}

type IndexField struct {
	Name       string
	Number     int
	Uppercase  bool
	Descending bool
}
