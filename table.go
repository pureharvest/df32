package df32

import (
	"io"
	"strings"
)

type Table struct {
	Number                  int64 `json:"file_num"`
	Name                    string
	System                  bool
	DisplayName             string `json:"display_name"`
	RecordLength            int64
	MaxRecords              int64
	FileCompression         string
	ReuseDeletedSpace       bool
	LockingType             string
	HeaderIntegrityChecking bool
	TransactionType         string
	RecordIdentityIndex     string
	FileLoginParameter      string
	Fields                  []Field
	Indexes                 []Index
}

func (t Table) WriteFileDefinition(w io.Writer) error {
	fd := t.FileDefinition()

	return fd.WriteTo(w)
}

func (t Table) FileDefinition() FileDefinition {
	fields := make([]FieldDefinition, 0, len(t.Fields)+1)

	offset := 0

	if len(t.Fields) > 0 && !strings.EqualFold(t.Fields[0].Name, "recnum") {
		offset = 1
		fields = append(fields, FieldDefinition{Number: 0, Name: "RECNUM", Type: "N"})
	}

	fd := FileDefinition{
		Number: int(t.Number),
		Name:   t.Name,
		Fields: fields,
	}

	for i, field := range t.Fields {
		fd.Fields = append(fd.Fields, FieldDefinition{Number: i + offset, Name: field.Name, Type: convertToFileDefinitionType(field.Type)})
	}

	return fd
}
