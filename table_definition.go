package df32

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"regexp"
	"strconv"
	"strings"
	"text/template"
	"time"
)

var sectionSplitter = `-----------------------------------------------------------------------------`

func NewTableFromDefinition(r io.Reader) (Table, error) {
	var err error

	table := Table{}

	scanner := bufio.NewScanner(r)

	if err := nextSection(scanner); err != nil {
		return table, errors.New("incomplete table definition")
	}

	table, err = scanHeader(scanner, table)

	if err != nil {
		return table, err
	}

	if err := nextSection(scanner); err != nil {
		return table, errors.New("incomplete table definition")
	}

	table, err = scanFileListInfo(scanner, table)

	if err != nil {
		return table, err
	}

	table, err = scanTableInfo(scanner, table)

	if err != nil {
		return table, err
	}

	table, err = scanFields(scanner, table)

	if err != nil {
		return table, err
	}

	table, err = scanIndexes(scanner, table)

	if err != nil {
		return table, err
	}

	return table, nil

}

func nextSection(scanner *bufio.Scanner) error {
	if !scanner.Scan() {
		return scanner.Err()
	}

	if scanner.Text() != sectionSplitter {
		return errors.New("unexpected input")
	}

	return nil
}

func scanHeader(scanner *bufio.Scanner, table Table) (Table, error) {
	var day, month, year, hour, minute, page int

	if !scanner.Scan() {
		return table, errors.New("incomplete header")
	}

	timestamp := scanner.Text()

	_, err := fmt.Sscanf(
		timestamp,
		" DATE: %d/%d/%d TIME: %d:%d PAGE: %d",
		&day,
		&month,
		&year,
		&hour,
		&minute,
		&page,
	)

	if err != nil {
		return table, errors.New("invalid header")
	}

	if !scanner.Scan() {
		return table, errors.New("incomplete header")
	}

	_, err = fmt.Sscanf(
		scanner.Text(),
		" FILE DEFINITION FOR FILE: %s (# %d)",
		&table.Name,
		&table.Number,
	)

	if err != nil {
		return table, errors.New("invalid header")
	}

	return table, nil
}

func scanFileListInfo(scanner *bufio.Scanner, table Table) (Table, error) {

	for scanner.Scan() {
		line := scanner.Text()

		if line == sectionSplitter {
			return table, nil
		}

		name, value, err := scanP(line)

		if err != nil {
			return table, err
		}

		switch name {
		case "USER DISPLAY NAME":
			table.DisplayName = value
		}
	}

	if err := scanner.Err(); err != nil {
		return table, err
	}

	return table, errors.New("unexpeced EOF")
}

func scanTableInfo(scanner *bufio.Scanner, table Table) (Table, error) {
	for scanner.Scan() {
		line := scanner.Text()

		if line == sectionSplitter {
			return table, nil
		}

		name, value, err := scanP(line)

		if err != nil {
			return table, err
		}

		switch name {
		case "FILE COMPRESSION":
			table.FileCompression = value
		case "LOCKING TYPE":
			table.LockingType = value
		case "TRANSACTION TYPE":
			table.TransactionType = value
		case "RECORD IDENTITY INDEX":
			table.RecordIdentityIndex = value
		case "FILE LOGIN PARAMETER":
			table.FileLoginParameter = value
		case "RECORD LENGTH":
			var used int64

			n, err := fmt.Sscanf(value, "%d ( USED: %d )", &table.RecordLength, &used)

			if err != nil {
				return table, err
			}

			if n != 2 {
				return table, errors.New("invalid format for record length")
			}
		case "MAX NUMBER OF RECORDS":
			var used int64

			n, err := fmt.Sscanf(value, "%d ( USED: %d )", &table.MaxRecords, &used)

			if err != nil {
				return table, err
			}

			if n != 2 {
				return table, errors.New("invalid format for record length")
			}

		case "RE-USE DELETED SPACE":
			v, err := parseBool(value)

			if err != nil {
				return table, err
			}
			table.ReuseDeletedSpace = v
		case "HEADER INTEGRITY CHECKING":
			v, err := parseBool(value)

			if err != nil {
				return table, err
			}
			table.HeaderIntegrityChecking = v
		case "SYSTEM FILE":
			v, err := parseBool(value)

			if err != nil {
				return table, err
			}
			table.System = v
		}
	}

	if err := scanner.Err(); err != nil {
		return table, err
	}

	return table, errors.New("unexpeced EOF")
}

func scanFields(scanner *bufio.Scanner, table Table) (Table, error) {

	if err := assertLine(scanner, "NUM  FIELD NAME       TYPE SIZE  OFFST IX   RELATES TO FILE.FIELD", true); err != nil {
		return table, err
	}

	if err := assertLine(scanner, "---  ---------------  ---- ----- ----- --   ---------------------------------", true); err != nil {
		return table, err
	}

	for scanner.Scan() {

		if scanner.Text() == "" {
			break
		}

		field, err := scanField(scanner.Text())

		if err != nil {
			return table, err
		}

		table.Fields = append(table.Fields, field)
	}

	return table, scanner.Err()
}

var fieldDefRegexp = regexp.MustCompile(`[ ]*(\d+) +(\w+) +(\w+) +(\d+\.?\d*) +(\d+) *(\d+)?( +(\w+)\.(\w+) \((\d+),(\d+)\))?`)

func scanField(data string) (Field, error) {

	matches := fieldDefRegexp.FindStringSubmatch(data)

	if len(matches) == 0 {
		return Field{}, errors.New("invalid field definition")
	}

	number, err := strconv.Atoi(matches[1])

	if err != nil {
		return Field{}, err
	}

	field := Field{
		Number: number,
		Name:   matches[2],
	}
	field.Type, err = parseFieldType(matches[3])

	if err != nil {
		return field, err
	}

	length, precision, err := parseFieldSize(matches[4])

	if err != nil {
		return field, err
	}

	offset, err := strconv.Atoi(matches[5])

	if err != nil {
		return field, err
	}

	field.Offset = offset

	field.Length = length
	field.Precision = precision

	if matches[6] != "" {
		index, err := strconv.Atoi(matches[6])

		if err != nil {
			return Field{}, err
		}

		field.Index = index

	}

	if matches[7] != "" {
		relFile, err := strconv.Atoi(matches[10])

		if err != nil {
			return field, err
		}
		relField, err := strconv.Atoi(matches[11])

		if err != nil {
			return field, err
		}

		field.RelatesTo = FieldRelation{
			File:      relFile,
			Field:     relField,
			FileName:  strings.Trim(matches[8], " "),
			FieldName: strings.Trim(matches[9], " "),
		}
	}

	return field, nil
}

func scanIndexes(scanner *bufio.Scanner, table Table) (Table, error) {

	if err := assertLine(scanner, "INDEX# FIELDS          DES U/C    LENGTH LEVELS SEGMENTS MODE", true); err != nil {
		return table, err
	}

	if err := assertLine(scanner, "------ --------------- --- ---    ------ ------ -------- -------", true); err != nil {
		return table, err
	}

	var currentIndex Index

	for scanner.Scan() {

		if scanner.Text() == "" {
			continue
		}

		matches := indexDefRegexp.FindStringSubmatch(scanner.Text())

		if len(matches) > 0 {
			if currentIndex.Number != 0 {
				table.Indexes = append(table.Indexes, currentIndex)
			}

			// Ignore error because regexp only matches digits.
			// If index number overflows int we have bigger problems
			number, _ := strconv.Atoi(matches[1])
			length, _ := strconv.Atoi(matches[5])
			levels, _ := strconv.Atoi(matches[6])
			currentIndex = Index{
				Number: number,
				Mode:   matches[8],
				Length: int64(length),
				Levels: int64(levels),
			}

		}

		field, err := scanIndexField(scanner.Text())

		if err != nil {
			return table, err
		}

		field.Number = len(currentIndex.Fields) + 1

		currentIndex.Fields = append(currentIndex.Fields, field)
	}

	if currentIndex.Number != 0 {
		table.Indexes = append(table.Indexes, currentIndex)
	}

	return table, scanner.Err()
}

var indexDefRegexp = regexp.MustCompile(`[ ]*(\d+) +(\w+) +(YES|NO) +(YES|NO) *(\d+)? *(\d+)? *(\d+)? *(.+)?`)
var indexFieldDefRegexp = regexp.MustCompile(`\s*(\w+) +(YES|NO) +(YES|NO)`)

func scanIndexField(data string) (IndexField, error) {
	matches := indexFieldDefRegexp.FindStringSubmatch(data)

	if len(matches) == 0 {
		return IndexField{}, errors.New("invalid index field definition")
	}

	// Ignore error because regexp only matches valid input.

	descending, _ := parseBool(matches[2])
	uppercase, _ := parseBool(matches[3])

	field := IndexField{
		Name:       strings.TrimRight(matches[1], " "),
		Uppercase:  uppercase,
		Descending: descending,
	}

	return field, nil

}

func scanP(data string) (name string, value string, err error) {

	parts := strings.SplitN(data, ":", 2)

	if len(parts) != 2 {
		return "", "", errors.New("invalid parameter line")
	}

	return strings.Trim(parts[0], " "), strings.Trim(parts[1], " "), nil
}

// assertLine scans once and asserts that the line is as expected.
func assertLine(scanner *bufio.Scanner, line string, ignoreEmpty bool) error {

	for scanner.Scan() {

		if scanner.Text() == line {
			return nil
		}

		if scanner.Text() != "" || !ignoreEmpty {
			return fmt.Errorf("unexpected line: %s expected %s", scanner.Text(), line)
		}
	}

	err := scanner.Err()

	if err != nil {

		return err
	}

	return errors.New("unexpected EOF")

}

func parseBool(value string) (bool, error) {
	if strings.ToUpper(value) == "YES" {
		return true, nil
	}

	if strings.ToUpper(value) == "NO" {
		return false, nil
	}

	return false, errors.New("invalid value for bool. must be YES or NO")

}

func parseFieldType(value string) (Type, error) {
	switch strings.ToUpper(value) {
	case "ASC":
		return AsciiType, nil
	case "NUM":
		return NumberType, nil
	case "DAT":
		return DateType, nil

	}

	return "unknown", errors.New("invalid dataflex type")
}

func parseFieldRelation(value string) (FieldRelation, error) {
	var tableName, fieldName string
	var tableNumber, fieldNumber int

	n, err := fmt.Sscanf(value, "%s.%s (%d,%d)", &tableName, &fieldName, &tableNumber, &fieldNumber)

	if err != nil {
		return FieldRelation{}, err
	}

	if n != 4 {
		return FieldRelation{}, errors.New("invalid field relation")

	}

	return FieldRelation{
		File:      tableNumber,
		Field:     fieldNumber,
		FieldName: fieldName,
		FileName:  tableName,
	}, nil
}

type defTemplateData struct {
	Date  string
	Time  string
	Table Table
}

const defTemplate = `-----------------------------------------------------------------------------
  DATE: {{ .Date }}      TIME: {{ .Time }}                                  PAGE:  1
  FILE DEFINITION FOR FILE: {{.Table.Name}} (# {{.Table.Number}})
-----------------------------------------------------------------------------
  DRIVER NAME               : DATAFLEX
  FILE ROOT NAME            : {{.Table.Name}}
  USER DISPLAY NAME         : {{.Table.DisplayName}}
  DATAFLEX FILE NAME        : {{.Table.Name}}
-----------------------------------------------------------------------------
  RECORD LENGTH             : {{.Table.RecordLength}}        ( USED: 0 )
  MAX NUMBER OF RECORDS     : {{.Table.MaxRecords}}    ( USED: 0 )
  FILE COMPRESSION          : {{.Table.FileCompression}}
  RE-USE DELETED SPACE      : {{formatBool .Table.ReuseDeletedSpace}}
  LOCKING TYPE              : {{.Table.LockingType}}
  HEADER INTEGRITY CHECKING : {{formatBool .Table.HeaderIntegrityChecking}} 
  TRANSACTION TYPE          : {{.Table.TransactionType}}
  RECORD IDENTITY INDEX     : {{.Table.RecordIdentityIndex}}
  FILE LOGIN PARAMETER      : {{.Table.FileLoginParameter}}
  SYSTEM FILE               : {{formatBool .Table.System}} 
-----------------------------------------------------------------------------

NUM  FIELD NAME       TYPE SIZE  OFFST IX   RELATES TO FILE.FIELD
---  ---------------  ---- ----- ----- --   ---------------------------------
{{ range $index, $field := .Table.Fields -}}
{{ writeField $field }}
{{ end }}

INDEX# FIELDS          DES U/C    LENGTH LEVELS SEGMENTS MODE
------ --------------- --- ---    ------ ------ -------- -------
{{ range .Table.Indexes -}}
{{ writeIndex . }}

{{ end -}}
`

func formatBool(value bool) string {
	if value {
		return "YES"
	}

	return "NO"
}

func WriteTableDefinition(w io.Writer, table Table, t time.Time) error {

	funcMap := template.FuncMap{

		"formatBool": formatBool,
		"writeField": TableFieldDefinitionString,
		"writeIndex": TableIndexDefinitionString,
	}

	data := defTemplateData{
		Table: table,
		Date:  t.Format("02/01/2006"),
		Time:  t.Format("15:04"),
	}

	tmpl := template.Must(template.New("defTempl").Funcs(funcMap).Parse(defTemplate))

	err := tmpl.Execute(w, data)

	return err
}

func TableFieldDefinitionString(f Field) string {
	var index string

	if f.Index != 0 {
		index = strconv.Itoa(f.Index)
	}

	return fmt.Sprintf(
		`%3d  %-15s  %-4s %5s %5d %2s   %s`,
		f.Number,
		f.Name,
		convertTypeToTableDefinition(f.Type),
		f.SizeString(),
		f.Offset,
		index,
		f.RelatesTo.String(),
	)
}

func TableIndexDefinitionString(index Index) string {
	output := fmt.Sprintf(
		"  %-4d %-14s  %-3s %-3s      %-4d   %-4d    %-5d %7s",
		index.Number,
		index.Fields[0].Name,
		formatBool(index.Fields[0].Descending),
		formatBool(index.Fields[0].Uppercase),
		index.Length,
		index.Levels,
		len(index.Fields),
		index.Mode,
	)

	if len(index.Fields) > 1 {
		for _, field := range index.Fields[1:] {
			output += fmt.Sprintf("\n       %-14s  %-3s %-3s", field.Name, formatBool(field.Descending), formatBool(field.Uppercase))
		}
	}

	return output
}
