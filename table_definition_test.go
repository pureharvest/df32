package df32

import (
	"fmt"
	"os"
	"strings"
	"testing"
	"time"

	"bytes"
	"io/ioutil"

	"github.com/stretchr/testify/assert"
)

func Test_ScanP(t *testing.T) {
	tests := []struct {
		input string
		name  string
		value string
		err   error
	}{
		{"DATAFLEX FILE NAME        : BOM", "DATAFLEX FILE NAME", "BOM", nil},
		{"  RECORD LENGTH             : 85        ( USED: 85 )", "RECORD LENGTH", "85        ( USED: 85 )", nil},
	}

	for i, test := range tests {
		t.Run(fmt.Sprintf("scanP %d", i), func(t *testing.T) {
			name, value, err := scanP(test.input)

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.value, value)
			assert.Equal(t, test.name, name)
		})
	}
}

func Test_ScanField(t *testing.T) {
	tests := []struct {
		input    string
		expected Field
		err      error
	}{
		{
			"  1  STOCK            ASC     20     1  2   STK.FIELD_1 (110,1)",
			Field{
				Number:    1,
				Name:      "STOCK",
				Type:      AsciiType,
				Length:    20,
				Offset:    1,
				RelatesTo: FieldRelation{110, 1, "STK", "FIELD_1"},
				Index:     2,
			},
			nil,
		},
		{
			"3  QTY              NUM   10.6    41      ",
			Field{
				Number:    3,
				Name:      "QTY",
				Type:      NumberType,
				Length:    10,
				Precision: 6,
				Offset:    41,
			},
			nil,
		},
	}

	for i, test := range tests {
		t.Run(fmt.Sprintf("scanField %d", i), func(t *testing.T) {
			field, err := scanField(test.input)

			assert.Equal(t, test.err, err)
			assert.Equal(t, test.expected, field)
		})
	}
}

func Test_WriteField(t *testing.T) {
	tests := []struct {
		expected string
		input    Field
		err      error
	}{
		{
			"  1  STOCK            ASC     20     1  2   STK.FIELD_1 (110,1)",
			Field{
				Number:    1,
				Name:      "STOCK",
				Type:      AsciiType,
				Length:    20,
				Offset:    1,
				RelatesTo: FieldRelation{110, 1, "STK", ""},
				Index:     2,
			},
			nil,
		},
		{
			"  3  QTY              NUM   10.6    41      ",
			Field{
				Number:    3,
				Name:      "QTY",
				Type:      NumberType,
				Length:    10,
				Precision: 6,
				Offset:    41,
			},
			nil,
		},
	}

	for i, test := range tests {
		t.Run(fmt.Sprintf("scanField %d", i), func(t *testing.T) {
			output := TableFieldDefinitionString(test.input)

			assert.Equal(t, test.expected, output)
		})
	}
}

func Test_TableDefinition(t *testing.T) {

	expected := Table{
		Name:                    "BOM",
		Number:                  135,
		DisplayName:             "Bill Of Materials File",
		RecordLength:            85,
		MaxRecords:              900000,
		FileCompression:         "NONE",
		ReuseDeletedSpace:       true,
		LockingType:             "FILE",
		HeaderIntegrityChecking: false,
		TransactionType:         "CLIENT ATOMIC",
		RecordIdentityIndex:     "0 ( 0 , 0 )",
		FileLoginParameter:      "",
		System:                  false,
		Fields: []Field{
			Field{Number: 1, Name: "STOCK", Type: AsciiType, Length: 20, Precision: 0, Offset: 1, Index: 2, RelatesTo: FieldRelation{110, 1, "STK", "FIELD_1"}},
			Field{Number: 2, Name: "SUB_STOCK", Type: AsciiType, Length: 20, Precision: 0, Offset: 21, Index: 2, RelatesTo: FieldRelation{110, 1, "STK", "FIELD_1"}},
			Field{Number: 3, Name: "QTY", Type: NumberType, Length: 10, Precision: 6, Offset: 41},
			Field{Number: 4, Name: "DESCRIPTION", Type: AsciiType, Length: 35, Precision: 0, Offset: 49},
			Field{Number: 5, Name: "MAINRAWMATERIAL", Type: NumberType, Length: 4, Precision: 0, Offset: 84},
		},
		Indexes: []Index{
			Index{
				Number: 1,
				Fields: []IndexField{
					IndexField{Number: 1, Name: "STOCK", Uppercase: true, Descending: false},
					IndexField{Number: 2, Name: "SUB_STOCK", Uppercase: true, Descending: false},
				},
				Length: 40,
				Levels: 6,
				Mode:   "ON-LINE",
			},
			Index{
				Number: 2,
				Fields: []IndexField{
					IndexField{Number: 1, Name: "SUB_STOCK", Uppercase: true, Descending: false},
					IndexField{Number: 2, Name: "STOCK", Uppercase: true, Descending: false},
				},
				Length: 40,
				Levels: 6,
				Mode:   "ON-LINE",
			},
		},
	}

	defFile, err := os.Open("./test-fixtures/BOM.def")
	if err != nil {
		t.Fatal(err)
	}

	table, err := NewTableFromDefinition(defFile)

	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, expected, table)

}

func Test_WriteTableDefinition(t *testing.T) {

	table := Table{
		Name:                    "BOM",
		Number:                  135,
		DisplayName:             "Bill Of Materials File",
		RecordLength:            85,
		MaxRecords:              900000,
		FileCompression:         "NONE",
		ReuseDeletedSpace:       true,
		LockingType:             "FILE",
		HeaderIntegrityChecking: false,
		TransactionType:         "CLIENT ATOMIC",
		RecordIdentityIndex:     "0 ( 0 , 0 )",
		FileLoginParameter:      "",
		System:                  false,
		Fields: []Field{
			Field{Number: 1, Name: "STOCK", Type: AsciiType, Length: 20, Precision: 0, Offset: 1, Index: 2, RelatesTo: FieldRelation{110, 1, "STK", "FIELD_1"}},
			Field{Number: 2, Name: "SUB_STOCK", Type: AsciiType, Length: 20, Precision: 0, Offset: 21, Index: 2, RelatesTo: FieldRelation{110, 1, "STK", "FIELD_1"}},
			Field{Number: 3, Name: "QTY", Type: NumberType, Length: 10, Precision: 6, Offset: 41},
			Field{Number: 4, Name: "DESCRIPTION", Type: AsciiType, Length: 35, Precision: 0, Offset: 49},
			Field{Number: 5, Name: "MAINRAWMATERIAL", Type: NumberType, Length: 4, Precision: 0, Offset: 84},
		},
		Indexes: []Index{
			Index{
				Number: 1,
				Fields: []IndexField{
					IndexField{Number: 1, Name: "STOCK", Uppercase: true, Descending: false},
					IndexField{Number: 2, Name: "SUB_STOCK", Uppercase: true, Descending: false},
				},
				Length: 40,
				Levels: 6,
				Mode:   "ON-LINE",
			},
			Index{
				Number: 2,
				Fields: []IndexField{
					IndexField{Number: 1, Name: "SUB_STOCK", Uppercase: true, Descending: false},
					IndexField{Number: 2, Name: "STOCK", Uppercase: true, Descending: false},
				},
				Length: 40,
				Levels: 6,
				Mode:   "ON-LINE",
			},
		},
	}

	expected, err := ioutil.ReadFile("./test-fixtures/BOM.def")

	if err != nil {
		t.Fatal(err)
	}

	var buf bytes.Buffer

	err = WriteTableDefinition(&buf, table, time.Date(2016, 9, 16, 11, 10, 0, 0, time.Local))
	if err != nil {
		t.Fatal(err)
	}
	assert.Equal(t, strings.Split(string(expected), "\n"), strings.Split(buf.String(), "\n"))

}
