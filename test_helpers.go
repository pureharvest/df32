package df32

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

func CreateTestEnvironment(t *testing.T, includePaths []string) (env Environment, cleanUp func()) {

	tmpdir, err := ioutil.TempDir(os.TempDir(), "df")

	if err != nil {
		t.Fatal(err)
	}

	cleanUp = func() {
		os.RemoveAll(tmpdir)
	}

	// Create an empty filelist to use as default
	list, _ := NewFilelist(filepath.Join(tmpdir, "filelist.cfg"))

	if err := list.Save(); err != nil {
		cleanUp()
		t.Fatal(err)
	}

	env = NewEnvironment(append(includePaths, tmpdir)...)

	if err != nil {
		cleanUp()
		t.Fatal(err)
	}

	return env, cleanUp
}
