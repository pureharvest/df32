package df32

import (
	"database/sql/driver"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/golang-sql/civil"
	"github.com/shopspring/decimal"
)

type ASCII string

func (t *ASCII) Scan(src interface{}) error {
	switch v := src.(type) {
	case time.Time:
		tt := v.Format(time.RFC3339)
		*t = ASCII(tt)
	case string:
		*t = ASCII(v)
	case []byte:
		tt := string(v)
		*t = ASCII(tt)
	case int64:
		tt := strconv.FormatInt(v, 10)
		*t = ASCII(tt)
	case float64:
		tt := strconv.FormatFloat(v, 'f', -1, 32)
		*t = ASCII(tt)
	case nil:
		*t = ASCII("")
	default:
		return fmt.Errorf("unsupported data %v", src)
	}

	return nil
}

func (t ASCII) Value() (driver.Value, error) {
	return driver.Value(string(t)), nil
}

type Date struct {
	civil.Date
}

func (d Date) IsNull() bool {
	return d.Year == 1 && d.Month == 1 && d.Day == 1
}

func (d *Date) Scan(src interface{}) error {
	switch v := src.(type) {
	case time.Time:
		d.Date = civil.DateOf(v)
	case string:
		dd, err := civil.ParseDate(v)

		if err != nil {
			return err
		}

		d.Date = dd
	case nil:
		d.Date = civil.Date{}
	default:
		return fmt.Errorf("unsupported data %v", src)
	}

	return fmt.Errorf("unsupported data %v", src)
}

func (t Date) Value() (driver.Value, error) {
	return driver.Value(t.Date.In(time.Local)), nil
}

type Text string

func (t *Text) Scan(src interface{}) error {
	switch v := src.(type) {
	case time.Time:
		tt := v.Format(time.RFC3339)
		*t = Text(tt)
	case string:
		*t = Text(v)
	case []byte:
		tt := string(v)
		*t = Text(tt)
	case int64:
		tt := strconv.FormatInt(v, 10)
		*t = Text(tt)
	case float64:
		tt := strconv.FormatFloat(v, 'f', -1, 32)
		*t = Text(tt)
	case nil:
		*t = Text("")
	default:
		return fmt.Errorf("unsupported data %v", src)
	}

	return nil
}

func (t Text) Value() (driver.Value, error) {
	return driver.Value(string(t)), nil
}

type Number decimal.Decimal

type Binary []byte

type Type string

const (
	AsciiType  Type = "ascii"
	DateType        = "date"
	NumberType      = "number"
	TextType        = "text"
	BinaryType      = "binary"
)

func convertTypeToTableDefinition(t Type) string {
	switch t {
	case AsciiType:
		return "ASC"
	case NumberType:
		return "NUM"
	case DateType:
		return "DAT"
	case TextType:
		return "TEX"
	case BinaryType:
		return "BIN"
	}

	return "ERR"
}

type TypeMap map[Type]string

// ConvertSQLTypeToDataflex will take a SQL datatype and return the equivilent Dataflex
// type or an error if it can't be converted
func ConvertSQLTypeToDataflex(t string) (Type, error) {
	t = strings.ToLower(t)
	switch t {
	case "ascii":
		return AsciiType, nil
	case "number":
		return NumberType, nil
	case "bytea":
		fallthrough
	case "binary":
		return BinaryType, nil
	case "char":
		fallthrough
	case "nchar":
		fallthrough
	case "varchar":
		fallthrough
	case "character varying":
		fallthrough
	case "nvarchar":
		return AsciiType, nil
	case "decimal":
		fallthrough
	case "int":
		fallthrough
	case "smallint":
		fallthrough
	case "numeric":
		return NumberType, nil
	case "text":
		fallthrough
	case "ntext":
		return TextType, nil
	case "date":
		fallthrough
	case "datetime":
		fallthrough
	case "datetime2":
		return DateType, nil
	}

	return "", errors.New("unsupportred type")
}

var DefaultDefaults = map[Type]string{
	AsciiType:  "''",
	DateType:   "'0001-01-01'",
	NumberType: "0",
	TextType:   "''",
}
