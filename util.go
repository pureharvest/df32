package df32

import (
	"bufio"
	"os"
	"strings"

	"io"

	"github.com/pkg/errors"
)

type ConfigLine struct {
	Name   string
	Value  string
	Number int
}

// readConfig will read a config line and return the key and value.
// Both default to blank if not present
func readConfigLine(line string) (key string, value string) {
	parts := strings.SplitN(line, " ", 2)

	key = ""
	value = ""

	if len(parts) > 0 {
		key = strings.ToUpper(strings.TrimSpace(parts[0]))

	}

	if len(parts) > 1 {
		value = strings.TrimSpace(parts[1])
	}

	return key, value
}

func ReadIntermediate(r io.Reader) ([]ConfigLine, error) {
	var items []ConfigLine
	scanner := bufio.NewScanner(r)

	lineNum := 0
	for scanner.Scan() {
		line := scanner.Text()
		lineNum++
		// Ignore comments and empty lines
		if strings.HasPrefix(line, ";") || strings.TrimSpace(line) == "" {
			continue
		}

		key, value := readConfigLine(line)

		if value == "" {
			return items, errors.Errorf("config error at line %d.", lineNum)
		}

		item := ConfigLine{
			Name:   key,
			Value:  value,
			Number: lineNum,
		}

		items = append(items, item)
	}

	return items, nil
}

func ReadIntermediateFile(filename string) ([]ConfigLine, error) {
	file, err := os.Open(filename)

	if err != nil {
		return nil, err
	}
	defer file.Close()

	return ReadIntermediate(file)
}
